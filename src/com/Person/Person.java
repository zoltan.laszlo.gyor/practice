package com.Person;

public class Person {
    private String name;
    private String address;
    protected long salary;

    public Person(String name, String address){
        this.name = name;
        this.address = address;
        this.salary = 30000;
    }

    public String getName() {
        return name;
    }

    public String getAddress(){
        return address;
    }

    @Override
    public String toString(){
        return name + "(" + address + ") wage: " + salary;
    }

    public void raise(){
        salary += 10000;
    }


}
