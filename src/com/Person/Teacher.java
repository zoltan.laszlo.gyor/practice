package com.Person;

import java.util.LinkedList;

public class Teacher extends Person{
    private LinkedList<String> courses;

    public Teacher (String name, String address){
        super(name, address); //ősosztály konstruktora
        courses = new LinkedList<>();
    }

    public void addCourse(String name){
        courses.add(name);
    }

    @Override
    public String toString(){
        return "Teacher: " + super.toString();
    }

    @Override
    public void raise(){
        salary += 10000;
    }
}
